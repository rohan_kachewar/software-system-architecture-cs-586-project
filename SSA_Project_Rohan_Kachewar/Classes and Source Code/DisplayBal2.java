
/**
 *
 * @author Rohan_Kachewar
 */
public class DisplayBal2 extends DisplayBalance {
    DataStore db;
    
    DisplayBal2(DataStore dbs)
    {
        db = dbs;
    }
    
    void displayBalance(){
        System.out.println("Current Account Balance is: " + db.getFloatBalance());
    }
    
}
