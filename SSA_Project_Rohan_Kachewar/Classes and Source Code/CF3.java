/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class CF3 extends AbstractFactory {
    DataStore db;
    
    public CF3() {
        db = getDataStore(); //get which data base to use at run time based on ATM
    }
                                
    public StoreData getStoreData(){
        StoreData sd = new SD3(db);           
        return sd;
    }

    DisplayMenu getDisplayMenuObject() {
        return new Display1();   //Reusing Display1 as display options are same
    }
    
    MakeDeposit getDepositObject() {
        return new MakeDepInt(db);
    }
    
    MakeWithdraw getWithdrawObject() {
        return new MakeWithdrawInt(db);
    }
    
    Penalty getPenaltyObject() {
        //return new Penalty3(db);
        return null; //As ATM3 has no penalty we return null here
    }
    
    DataStore getDataStore() {
        return new DB3();
    }
    
    DisplayBalance getDisplayBalanceObject() {
        return new DisplayBal1(db); //Reusing DisplayBal1 as integer balance
    }
}
