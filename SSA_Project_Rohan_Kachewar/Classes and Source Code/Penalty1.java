

/**
 *
 * @author Rohan_Kachewar
 */
public class Penalty1 extends Penalty{
    DataStore db;
    int penalty;
    Penalty1(DataStore dbs) {
        db = dbs;
        penalty = 10;
    }
 
    public void penalty()
    {               
        int balance=db.getBalance();   //get existing balance
        int totalamt=balance -penalty;
        db.setBalance(totalamt);  //set new balance
    }            
}
