
/**
 *
 * @author Rohan_Kachewar
 */
public class MakeWithdrawInt extends MakeWithdraw {
    DataStore db;
    
    MakeWithdrawInt(DataStore dbs) {
        db = dbs;
    }
    
    public void makeWithdraw()
    {
        DataStore tempDB=new TempDataStore();
        int tempWithdraw=tempDB.getIntWithdraw();   //get withdraw amnt from temp db
        //DataStore db=new DB1(); 
        int balance=db.getBalance();   //get existing balance
        int totalamt=balance - tempWithdraw; //withdraw amount        
        db.setBalance(totalamt);  //set new balance
    }
    
    
}
