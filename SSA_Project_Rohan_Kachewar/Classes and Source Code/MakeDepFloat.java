
import java.math.BigDecimal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class MakeDepFloat extends MakeDeposit{
    DataStore db; 
    
    MakeDepFloat(DataStore dbs) {
       db = dbs;
    }
    
    
    public void makeDeposit()
    {
        DataStore tempDB=new TempDataStore();
        float tempDeposit=tempDB.getFloatDeposit();   //get deposit amnt        
        float balance=db.getFloatBalance();   //get existing balance
        //Use of BigDecimal ensures less error prone floating point operations
        BigDecimal foo1 = new BigDecimal(balance);
        BigDecimal foo2 = new BigDecimal(tempDeposit);
        BigDecimal foo3=foo1.add(foo2); //withdraw amount
        float totalamt = foo3.floatValue();                        
        db.setBalance(totalamt);  //set new balance
    }
}
