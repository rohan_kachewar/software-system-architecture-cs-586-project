/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class DisplayBal1 extends DisplayBalance {
    DataStore db;

    DisplayBal1(DataStore dbs) {
        db = dbs;
    }
               
    void displayBalance(){
        System.out.println("Current Account Balance is: " + db.getBalance());
    }
    
}
