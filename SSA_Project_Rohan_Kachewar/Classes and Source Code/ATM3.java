
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * @author Rohan_Kachewar
 */
public class ATM3 {
    DataStore tob, db3;
    MDA m;
    int maxval;//Max Pin Attempts    
    BufferedReader b;
    
    public ATM3() {            
    tob=new TempDataStore();
    tob.setATM_ID(3); //Set ATM ID to 3
    db3=new DB3();
    maxval = 1;
    b=new BufferedReader(new InputStreamReader(System.in));
    }

           
    public void card (int x, int y) {    
        tob.setBalance(x);
        tob.setPIN(y);    
        m.card();
    } 
        
            
    
    public void pin(int userEnteredPin) throws IOException{
        
        int temp5 = db3.getIntPIN();                        
        if(userEnteredPin != temp5)
        {            
          m.incorrectPin(maxval);
        } else {                                
        int tempBal=db3.getBalance();        
        if(tempBal<100)
        {
            m.correctPinBelowMin();
        }
        else
        {
            m.correctPinAboveMin();
        }            
      }
    }
  
    
 public void deposit(int damt) throws IOException
 {    
    tob.setIntDeposit(damt);
    m.deposit();
    int temp1 = db3.getBalance();        
    if(temp1<100)
    {        
        m.belowMinBalance();
    }
    else
    {     
        m.aboveMinBalance();
    }
 }
    
  
 public void withdraw(int withdrawamt) throws IOException
 {
    
    tob.setIntWithdraw(withdrawamt);        
    int temp2 = db3.getBalance(); 

    if(temp2>(withdrawamt)) // if less than or equal to previous balance
    {
        m.withdraw();
    }
    temp2 = db3.getBalance(); //get fresh balance after withdraw
    if(temp2<100)
    {     
        m.belowMinBalance(); 
    }
    else
    {     
        m.aboveMinBalance();
    }
 }
    
 public void lock() throws IOException{            
    m.lock();    
 }
 
 public void unlock() throws IOException{            
        m.unlock();
        int tempBalance = db3.getBalance();
        if(tempBalance<100)
        {
            m.belowMinBalance();
        }
        else
        {
            m.aboveMinBalance();
        }    
 } 
 public void balance(){m.balance();}
 public void exit(){m.exit();}
 
public void service()
{
 try{
                   
    m=new MDA();
    m.create();
    
    String inp=null; 
    String userEnteredPin;
    
    int temp,ch;    
            
    
    
    System.out.println("ATM3 MENU of Operations:"); 
    System.out.println("1. card(int x, int y)");
    System.out.println("2. pin(int x)");
    System.out.println("3. deposit(int x))");
    System.out.println("4. withdrawal(int x)");
    System.out.println("5. balance()");    
    System.out.println("6. exit()");
    System.out.println("7. lock()");
    System.out.println("8. unlock()");
    System.out.println("q. To Terminate from program");
    System.out.println("Please make a note of above operations");                
while (true)
    {    
    System.out.println("Select Operation:  \n 1:card 2:pin 3:deposit 4:withdraw 5:balance 6:exit 7:lock 8:unlock");            
    inp=b.readLine();
    ch=Integer.parseInt(inp);
    
    switch (ch)
    {        
    case 1:                  //deposit
        System.out.println("Operation: card(int x,int y)");        
        System.out.println("Enter value of parameter x");
        int tbal=Integer.parseInt(b.readLine());
        System.out.println("Enter value of parameter y");
        int tpin=Integer.parseInt(b.readLine());        
        card(tbal, tpin);
        break;        
    case 2:                  //deposit
        System.out.println("Operation: pin(int x)");        
        System.out.println("Enter value of parameter x");
        int pinFirst=Integer.parseInt(b.readLine());
        pin(pinFirst);    
        break;
    case 3:                  //deposit
        System.out.println("Operation: deposit(int x)");        
        System.out.println("Enter value of parameter x");
        int damt=Integer.parseInt(b.readLine());        
        deposit(damt);        
        break;
   case 4:                 //withdraw
        System.out.println("Operation: withdraw(int x)");        
        System.out.println("Enter value of parameter x");
        int withdrawamt=Integer.parseInt(b.readLine());
        withdraw(withdrawamt);
        break;
    case 5:               //balance
        System.out.println("Operation: balance()");                
        balance();
        break;    
    case 6:                      //exit
        System.out.println("Operation: exit()");                
        exit(); 
        break;
    case 7:                     //lock       
        System.out.println("Operation: lock()");                
        lock();
        break;
    case 8:                      //unlock    
        System.out.println("Operation: unlock()");                
        unlock();        
        break;    
    case 9:
        System.exit(0);
        break;
    default: 
    System.out.println("Incorrect choice");
      }
    }
  }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e.getMessage());
    }
   }

    
}
    

