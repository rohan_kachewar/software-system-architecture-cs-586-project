import java.io.*;
import java.lang.*;

/**
 * @author Rohan_Kachewar
 **/
public class ATM1 {
    DataStore tob, db1;
    MDA m;
    int attempts, maxval;//Max Pin Attempts    
    BufferedReader b;
    Boolean cardInserted, pinEntered;
    
    public ATM1() {            
    tob=new TempDataStore();
    tob.setATM_ID(1); //Set ATM ID to 1
    db1=new DB1();
    maxval = 3;
    b=new BufferedReader(new InputStreamReader(System.in));
    }

           
    public void card (int x, String y) {        
        tob.setBalance(x);
        tob.setPIN(y);    
        m.card();
        attempts = 0;
    } 
    
    
    
    public void pin(String userEnteredPin) throws IOException{
        Boolean correctPINFlag = false;        
        String temp5 = db1.getPIN();                        
        if(!userEnteredPin.equals(temp5))
        {                       
            m.incorrectPin(maxval);
        }else {                    
            int tempBal=db1.getBalance();            
            if(tempBal<1000)
            {
                m.correctPinBelowMin();
            }
            else
            {
                m.correctPinAboveMin();
            }
        } 
        
    }
  
    
 public void deposit() throws IOException
 {
    System.out.println("Please enter the amount to deposit: ");
    int damt=Integer.parseInt(b.readLine());        
    tob.setIntDeposit(damt);
    m.deposit();
    int temp1 = db1.getBalance();        
    if(temp1<1000)
    {        
        m.belowMinBalance();
    }
    else
    {     
        m.aboveMinBalance();
    }
 }
    
 
  
 public void withdraw(int withdrawamt) throws IOException
 {    
    tob.setIntWithdraw(withdrawamt);        
    int temp2 = db1.getBalance(); 
    if(temp2>(withdrawamt)) // if less than or equal to previous balance
    {
        m.withdraw();
    }
    temp2 = db1.getBalance(); //get fresh balance after withdraw
    if(temp2<1000)
    {        
        m.belowMinBalance(); 
    }
    else
    {     
        m.aboveMinBalance();
    }
 }
    
 public void lock(String tempPIN) throws IOException{
    
    String temp6=db1.getPIN();
    if(tempPIN.equals(temp6))
        {
          m.lock();
        }
 }
 
 public void unlock(String tempPIN7) throws IOException{    
    String temp7=db1.getPIN();
    if(tempPIN7.equals(temp7))
    {
        m.unlock();
        int tempBalance = db1.getBalance();
        if(tempBalance<1000)
        {
            m.belowMinBalance();
        }
        else
        {
            m.aboveMinBalance();
        }
    }
 } 
 public void balance(){m.balance();}
 public void exit(){m.exit();}
 
public void service()
{
 try{
                   
    m=new MDA();
    m.create();
    
    String inp=null, userEnteredPin;
    int enteredid;
    int temp,ch;    
    
    System.out.println("ATM1 MENU of Operations:"); 
    System.out.println("1. card(int x, string y)");
    System.out.println("2. pin(string x)");
    System.out.println("3. deposit(int x))");
    System.out.println("4. withdrawal(int x)");
    System.out.println("5. balance");    
    System.out.println("6. exit");
    System.out.println("7. lock(string x)");
    System.out.println("8. unlock(string x)");
    System.out.println("9. To Terminate from program");
    System.out.println("Please make a note of above operations");
                    
                
while (true)
    {    
        
    System.out.println("Select Operation:  \n 1:card 2:pin 3:deposit 4:withdraw 5:balance 6:exit 7:lock 8:unlock");            

    inp=b.readLine();
    ch=Integer.parseInt(inp);    
    switch (ch)
    {        
   case 1:                  //card
       System.out.println("Operation: card(int x,string y)");        
       System.out.println("Enter value of parameter x");
        int tbal=Integer.parseInt(b.readLine());
        System.out.println("Enter value of parameter y");
        String tpin=b.readLine();
        card(tbal, tpin);    
        break;
   case 2:                  //pin
       System.out.println("Operation: pin(string x)");
       System.out.println("Enter value of parameter x");
        //Now the action for PIN Prompt Msg has been triggered, so we will accept the PIN
        String pinFirst=b.readLine();
        pin(pinFirst);
        //displayMenuflag = false;
        break;
   case 3:                  //deposit
       System.out.println("Operation: deposit(int x)");        
       System.out.println("Enter value of parameter x");
        deposit();        
        break;
   case 4:                 //withdraw
        System.out.println("Operation: withdraw(int x)");        
        System.out.println("Enter value of parameter x");
        int withdrawamt=Integer.parseInt(b.readLine());
        withdraw(withdrawamt);
        break;
    case 5:               //balance
        System.out.println("Operation: balance()");        
        balance();
        break;    
    case 6:                      //exit
        System.out.println("Operation: exit()");        
        exit();              
        break;
    case 7:                     //lock
        System.out.println("Operation: lock(string x)");        
        System.out.println("Enter value of parameter x");
        String tempPIN=b.readLine();        
        lock(tempPIN);
        break;
    case 8:                      //unlock
        System.out.println("Operation: unlock(string x)");        
        System.out.println("Enter value of parameter x");
        String tempPIN7=b.readLine();        
        unlock(tempPIN7);        
        break;
    case 9:        
        System.exit(0);        
    break;
    default: 
    System.out.println("incorrect choice");
      }
    }
  }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e.getMessage());
    }
   }

    

    
}
