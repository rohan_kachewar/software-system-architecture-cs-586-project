
/**
 *
 * @author Rohan_Kachewar
 */
class S1 extends States {

    private int state_id;
    
    public S1() {
        state_id = 4;
    }
                   
    public int get_id()
    {
        return state_id;
    }
    
    
    public void penalty()
    {
        outputprocessorobj.penalty();
    }
    
}
