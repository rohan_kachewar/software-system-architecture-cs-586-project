
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 

/**
 * @author Rohan_Kachewar
 * Here We use the Centralized Approach for State Pattern Design
 */
public class MDA {
    static int state=00;
    States obj1=new Idle();         //1
    States obj2=new Check_PIN();    //2
    States obj3=new Ready();        //3      //with resepect to the current state the actions displayed
    States obj4=new S1();           //4  //in the menu are performed according to the input request
    States obj5=new Overdrawn();    //5    
    States obj6=new Locked();       //6
    States obj7=new S2();           //7
    
    States currObj; //Referance to hold current state
    int attempts;
    
    public MDA()
    {        
    }
    
    public void create()
    {
        //Change State to Idle state
        currObj = obj1;
    }

    public void card()
    {
        if(currObj.get_id() == 1)
        {   //Currently in Idle State            
            currObj.storeBalance();
            currObj.storePIN();             
            currObj.promptForPIN();
            attempts = 0;
            //Change State to Check Pin State
            currObj = obj2;
        }
    }

    
        
     public void incorrectPin(int maxval) 
    {                                                
           if(currObj.get_id() == 2) {
                currObj.pinFailed();                                                     
                if(attempts>=maxval)
                {                
                 currObj.too_many_attempts_msg();
                 currObj.eject_card();
                 //change the current state to Idle again
                 currObj = obj1;
                }          
                ++attempts;   
           }                
    }
    
    void correctPinBelowMin() {
        if(currObj.get_id() == 2) {
            //Current state is Check PIN
            currObj.display_menu();
                //Change State to overdrawn
            currObj = obj5;
        }
    }

    void correctPinAboveMin() {
        if(currObj.get_id() == 2) {
            //Current state is Check PIN
            currObj.display_menu();
            //Change State to Ready
            currObj = obj3;
        }
    }

    public void deposit() {        
        if(currObj.get_id() == 5)
        {
            //current in Overdrawn State
            currObj.deposit();
            //change State to S1
            currObj = obj4;
        }
        else if(currObj.get_id() == 3)
        {
            //current in Ready State
            currObj.deposit();
        }
    }

    
    
    public void withdraw() {        
        if(currObj.get_id() == 3)
        {   //current in Ready State            
            currObj.withdraw();
            //change State to S1
            currObj = obj4;
        } else {
            System.out.println("Cannot Withdraw if balance below minimum");
        }         
    }
    
    
    
    void belowMinBalance() {
        if(currObj.get_id() == 7)//If current state in S2
        {                        
            //Change State to Overdrawn
            currObj = obj5;            
        }
        if(currObj.get_id() == 4)//If current state in S1
        {                        
            currObj.penalty();
            //Change State to Overdrawn
            currObj = obj5;            
        } 
    }

    void aboveMinBalance() {
        if(currObj.get_id() == 7 || currObj.get_id() == 4)//If current state in S2 or S1
        {
            //Change State to Ready
            currObj = obj3;
        }
    }

//    void printCurrentState() {
//        //Used for debugging, to be removed later
//        System.out.println("Current State is: " + currObj.get_id());             
//    }

    void balance() {
        if(currObj.get_id() == 5)
        {   //current in Overdrawn State            
            currObj.displayBalance();            
        }
        else if(currObj.get_id() == 3)
        {   //current in Ready State            
            currObj.displayBalance();
        }
    }
    
    
    void exit() {
        if(currObj.get_id() == 5)
        {   //current in Overdrawn State            
            currObj.eject_card();
            //change state to Idle
            currObj = obj1; 
        }
        else if(currObj.get_id() == 3)
        {   //current in Ready State            
            currObj.eject_card();
            //change state to Idle
            currObj = obj1;
        }        
    }

    void lock() {
        if(currObj.get_id() == 5)
        {   //current in Overdrawn State            
            currObj.lock();
            //change state to Locked
            currObj = obj6; 
        }
        else if(currObj.get_id() == 3)
        {   //current in Ready State            
            currObj.lock();
            //change state to Locked
            currObj = obj6;
        }        
        
    }

    void unlock() {        
        if(currObj.get_id() == 6)
        {   //current in Locked State            
            currObj.unlock();
            //change state to S2
            currObj = obj7; 
        }        
    }

}
