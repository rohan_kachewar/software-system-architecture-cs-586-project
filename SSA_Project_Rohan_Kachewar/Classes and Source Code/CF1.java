
/**
 * @author Rohan_Kachewar
 */
public class CF1 extends AbstractFactory {
    DataStore db;
    
    public CF1() {
        db = getDataStore(); //get which data base to use at run time based on ATM
    }
                            
    
    public StoreData getStoreData(){
        StoreData sd = new SD1(db);           
        return sd;
    }

    DisplayMenu getDisplayMenuObject() {
        return new Display1();
    }
    
    MakeDeposit getDepositObject() {
        return new MakeDepInt(db);
    }
    
    MakeWithdraw getWithdrawObject() {
        return new MakeWithdrawInt(db);
    }
    
    Penalty getPenaltyObject() {
        return new Penalty1(db);
    }
    
    DataStore getDataStore() {
        return new DB1();
    }
    
    DisplayBalance getDisplayBalanceObject() {
        return new DisplayBal1(db);
    }

}

