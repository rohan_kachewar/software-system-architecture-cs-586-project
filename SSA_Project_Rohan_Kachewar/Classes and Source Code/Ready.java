/**
 *
 * @author Rohan_Kachewar
 */
class Ready extends States {

    private int state_id;
    
    public Ready() {
        state_id = 3;
    }
                   
    public int get_id()
    {
        return state_id;
    }
    
    public void deposit()
    {
        outputprocessorobj.makeDeposit();
    }
    
    public void withdraw()
    {
        outputprocessorobj.makeWithdraw();
    }
    
    public void displayBalance()
    {
        outputprocessorobj.displayBalance();
    }
    
    public void eject_card()
    {
        outputprocessorobj.eject_card();
    }
    
    public void lock()
    {
        outputprocessorobj.lock();
    }
}
