
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Rohan_Kachewar
 */
public class ATM2 {
    DataStore tob, db2;
    MDA m;
    int maxval;//Max Pin Attempts    
    BufferedReader b;
    
    public ATM2() {            
    tob=new TempDataStore();
    tob.setATM_ID(2); //Set ATM ID to 2
    db2=new DB2();
    maxval = 2;
    b=new BufferedReader(new InputStreamReader(System.in));
    }

           
    public void CARD (float x, int y) {        
        tob.setBalance(x);
        tob.setPIN(y);    
        m.card();
    } 
    
    
        
    public void PIN(int userEnteredPin) throws IOException{
        Boolean correctPINFlag = false;        
        int temp5 = db2.getIntPIN();                        
        if(userEnteredPin != temp5)
        {            
               m.incorrectPin(maxval);
        } else {                                    
            float tempBal=db2.getFloatBalance();            
            if(tempBal<500)
            {
                m.correctPinBelowMin();
            }
            else
            {
                m.correctPinAboveMin();
            }
        }                        
    }
  
    
 public void DEPOSIT(float damt) throws IOException
 {
    
    tob.setFloatDeposit(damt);
    m.deposit();
    float temp1 = db2.getFloatBalance();        
    if(temp1<500)
    {        
        m.belowMinBalance();
    }
    else
    {     
        m.aboveMinBalance();
    }
 }
    
 
  
 public void WITHDRAW(float withdrawamt) throws IOException
 {    
    tob.setFloatWithdraw(withdrawamt);        
    float temp2 = db2.getFloatBalance(); 
    if(temp2>(withdrawamt)) // if less than or equal to previous balance
    {
        m.withdraw();
    }
    temp2 = db2.getFloatBalance(); //get fresh balance after withdraw
    if(temp2<500)
    {    
        m.belowMinBalance(); 
    }
    else
    {     
        m.aboveMinBalance();
    }
 }

 public void BALANCE(){m.balance();}
 public void EXIT(){m.exit();}
 
public void service()
{
 try{
                   
    m=new MDA();
    m.create();    
    String inp=null; 
    String userEnteredPin;    
    int temp,ch;    
                        
    System.out.println("ATM2 MENU of Operations:"); 
    System.out.println("1. CARD(float x, int y)");
    System.out.println("2. PIN(int x)");
    System.out.println("3. DEPOSIT(float x)");    
    System.out.println("4. WITHDRAW(float x)");
    System.out.println("5. BALANCE()");    
    System.out.println("6. EXIT()");    
    System.out.println("7. To Terminate from program");
    System.out.println("Please make a note of above operations");        
    
 while (true)
    {    
    System.out.println("Select Operation:  \n 1:CARD 2:PIN 3:DEPOSIT 4:WITHDRAW 5:BALANCE 6:EXIT");                    
    inp=b.readLine();
    ch=Integer.parseInt(inp);    
    switch (ch)
    {                
    case 1:                  //CARD
        System.out.println("Operation: CARD(float x,int y)");        
        System.out.println("Enter value of parameter x");
        float tbal=Float.parseFloat(b.readLine());
        System.out.println("Enter value of parameter y");
        int tpin=Integer.parseInt(b.readLine());
        CARD(tbal, tpin);            
        break;
    case 2:                  //PIN        
        System.out.println("Operation: PIN(int x)");        
        System.out.println("Enter value of parameter x");
        int pinFirst=Integer.parseInt(b.readLine());
        PIN(pinFirst);        
        break;
    case 3:                  //deposit
        System.out.println("Operation: DEPOSIT(float x)");        
        System.out.println("Enter value of parameter x");
        float damt=Float.parseFloat(b.readLine());        
        DEPOSIT(damt);        
        break;
   case 4:                 //withdraw
        System.out.println("Operation: WITHDRAW(float x)");        
        System.out.println("Enter value of parameter x");
        float withdrawamt=Float.parseFloat(b.readLine());
        WITHDRAW(withdrawamt);
        break;
    case 5:               //balance
        System.out.println("Operation: BALANCE()");                
        BALANCE();
        break;    
    case 6:                      //exit
        System.out.println("Operation: EXIT()");                
        EXIT();           
        break;
    case 7:
        System.exit(0);
    break;
    default: 
    System.out.println("Incorrect choice");
      }
    }
  }
    catch(Exception e){
        e.printStackTrace();
        System.out.println(e.getMessage());
    }
   }

    
}
