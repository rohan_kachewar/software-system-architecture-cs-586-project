/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class Penalty2 extends Penalty{
    DataStore db;
    int penalty;
    Penalty2(DataStore dbs) {
        db = dbs;
        penalty = 20;
    }
 
    public void penalty()
    {               
        float balance=db.getFloatBalance();   //get existing balance
        float totalamt=balance -penalty;
        db.setBalance(totalamt);  //set new balance
    }            
}
