
/**
 *
 * @author Rohan_Kachewar
 */
public class OutputProcessor {
   DataStore tempobj;
   int ATM_ID;
   StoreData sdobj;
   AbstractFactory cf;
    
   public OutputProcessor()                      
	{
            tempobj=new TempDataStore();            
            ATM_ID = tempobj.getATM_ID();
            //Here we get the required concrete Factory based on ATM ID
            if(ATM_ID==1) {
                cf = new CF1();
            } else if(ATM_ID==2) {
                cf = new CF2();
            } else {
                cf = new CF3();
            }                                      
            
        }
   
    public void storePIN()
	{		
        sdobj=cf.getStoreData();
        sdobj.storePIN();
	}
    public void storeBalance()
	{		
        sdobj=cf.getStoreData();
        sdobj.storeBalance();
	}

    void too_many_attempts_msg() {
        TooManyAttemptsMsg tmsg = cf.getTooManyAttemptsMsgObj();
        tmsg.printTooManyAttemptsMsg();
        }
    
    void prompt_for_PIN() {
        PromptForPIN pmsg = cf.getPromptForPINObj();
        pmsg.promptPIN();
    }

    void incorrect_pin_msg() {
        IncorrectPinMsg imsg = cf.getIncorrectPinMsgObj();
        imsg.printIncorrectPinMsg();
    }
    
    public void display_menu()
	{	
	DisplayMenu dobj;			
        dobj=cf.getDisplayMenuObject();
        dobj.displaymenu();		
	}

    public void makeDeposit() {        
            MakeDeposit depo;            	
            depo=cf.getDepositObject();
            depo.makeDeposit();	            	
    }

    void penalty() {
        Penalty p=cf.getPenaltyObject();
        if (p!=null){p.penalty();}           
    }

    void makeWithdraw() {
        MakeWithdraw withdraw;            	
        withdraw=cf.getWithdrawObject();
        withdraw.makeWithdraw();
    }

     void displayBalance() {
        DisplayBalance displayBal;            	
        displayBal=cf.getDisplayBalanceObject();
        displayBal.displayBalance();
    }

    void eject_card() {
        EjectCard eject;            	
        eject=cf.getEjectCardObject();        
        eject.eject_card();
    }

    void lock() {
        System.out.println("Card Locked");
    }
    
    void unlock() {
        System.out.println("Card UN Locked");
    }
}
