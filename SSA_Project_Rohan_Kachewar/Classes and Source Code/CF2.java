/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class CF2 extends AbstractFactory {
    DataStore db;
    
    public CF2() {
        db = getDataStore(); //get which data base to use at run time based on ATM
    }
                            
    
    public StoreData getStoreData(){
        StoreData sd = new SD2(db);           
        return sd;
    }

    DisplayMenu getDisplayMenuObject() {
        return new Display2();
    }
    
    MakeDeposit getDepositObject() {
        return new MakeDepFloat(db);
    }
    
    MakeWithdraw getWithdrawObject() {
        return new MakeWithdrawFloat(db);
    }
    
    Penalty getPenaltyObject() {
        return new Penalty2(db);
    }
    
    DataStore getDataStore() {
        return new DB2();
    }
    
    DisplayBalance getDisplayBalanceObject() {
        return new DisplayBal2(db);
    }
}
