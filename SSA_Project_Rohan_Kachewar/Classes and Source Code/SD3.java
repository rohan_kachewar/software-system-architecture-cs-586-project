/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class SD3 extends StoreData {
    DataStore tempDB;
    DataStore db;
            
    public SD3(DataStore dbs) {
        tempDB = new TempDataStore();
        db = dbs;
    }
    
    public void storePIN()
    {        
        Integer pin =tempDB.getIntPIN();                
        db.setPIN(pin);        
    }
    
    public void storeBalance()
    {        
        int bal =tempDB.getBalance();                
        db.setBalance(bal);        
    }

}
