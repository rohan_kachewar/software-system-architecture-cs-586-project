import java.io.*;

/**
 *
 * @author Rohan_Kachewar
 */
public class ATM_Creator {        

    
    public static void main(String args[]) throws Exception
    {
    try{
        System.out.println("SELECT ATM: \n 1: ATM 1 \n 2: ATM 2 \n 3: ATM 3");
        BufferedReader b=new BufferedReader(new InputStreamReader(System.in));
        int ch=Integer.parseInt(b.readLine());
        switch (ch)
        {
            case 1: 
                System.out.println("ATM 1"); 
                ATM1 a1=new ATM1(); 
                a1.service();
                break;
            case 2: 
                System.out.println("ATM 2"); 
                ATM2 a2=new ATM2(); 
                a2.service();
                break;                
            case 3: 
                System.out.println("ATM 3"); 
                ATM3 a3=new ATM3(); 
                a3.service();
                break;
            default: System.out.println("wrong choice");
        }
       }
    catch(Exception e){
        System.out.println(e.getMessage());
        throw e;
        }
   }
}
    

