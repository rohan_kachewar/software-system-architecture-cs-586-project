
/**
 *
 * @author Rohan_Kachewar
 */
abstract class AbstractFactory {

    StoreData getStoreData() {return null;}

    DisplayMenu getDisplayMenuObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    MakeDeposit getDepositObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    PromptForPIN getPromptForPINObj() {
        return new PromptForPIN();
    }        
    IncorrectPinMsg getIncorrectPinMsgObj() {
        return new IncorrectPinMsg();
    }    
    TooManyAttemptsMsg getTooManyAttemptsMsgObj() {
        return new TooManyAttemptsMsg();
    }

    Penalty getPenaltyObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    MakeWithdraw getWithdrawObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    DataStore getDataStore() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    EjectCard getEjectCardObject() {
        return new EjectCard();
    }

    DisplayBalance getDisplayBalanceObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
       
}
