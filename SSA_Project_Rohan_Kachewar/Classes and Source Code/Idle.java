

/**
 *
 * @author Rohan_Kachewar
 */
class Idle extends States {

    private int state_id;
    
    public Idle() {
    state_id = 1;
    }
    
    public int get_id()
    {
        return state_id;
    }   
     
    public void storePIN()
    {outputprocessorobj.storePIN();}

    public void storeBalance()
    {outputprocessorobj.storeBalance();}
    
    void promptForPIN() {
        outputprocessorobj.prompt_for_PIN(); //exceeded max attempt        
    }
    
}
