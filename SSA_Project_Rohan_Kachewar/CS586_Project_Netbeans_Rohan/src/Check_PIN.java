
/**
 *
 * @author Rohan_Kachewar
 */
class Check_PIN extends States {
    private int state_id;
    
    
    public Check_PIN() {
        state_id = 2;
    }
                         
     public int get_id()
    {
        return state_id;
    }
     void pinFailed() {
         outputprocessorobj.incorrect_pin_msg(); //exceeded max attempt         
    }
    
     void too_many_attempts_msg() {
        outputprocessorobj.too_many_attempts_msg(); //exceeded max attempt
    }
    
     void display_menu() {
        outputprocessorobj.display_menu();//exceeded max attempt
    }     
    public void eject_card()
    {
        outputprocessorobj.eject_card();
    }
}
