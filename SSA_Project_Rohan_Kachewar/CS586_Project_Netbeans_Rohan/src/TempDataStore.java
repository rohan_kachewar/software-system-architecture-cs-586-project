/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class TempDataStore extends DataStore {
    static String tempStrPin;
    static int tempIntPin;        
    static int tempIntBal;
    static float tempFloatBal;    
    static int tempIntDeposit;
    static float tempFloatDeposit;    
    static int tempIntWithdraw;
    static float tempFloatWithdraw;    
    static int ATM_ID;
                    
            
    public void setATM_ID(int id)
        {
            ATM_ID=id;
        }
        
    public int getATM_ID()
        {
            return ATM_ID;
        }
            
    public void setPIN(String a) {tempStrPin = a;}   
    public void setPIN(int a) {tempIntPin = a;}   
    
    public String getPIN(){return tempStrPin;}
    public int getIntPIN(){return tempIntPin;}
    
    
    public void setBalance(int a2) {tempIntBal = a2;}   
    public void setBalance(float a2) {tempFloatBal = a2;}   
    
    public int getBalance(){return tempIntBal;}
    public float getFloatBalance(){return tempFloatBal;}
            
    public void setIntDeposit(int a2) 
    {
        tempIntDeposit = a2;
    }   
    public void setFloatDeposit(float a2) 
    {
        tempFloatDeposit = a2;
    }   
    public int getIntDeposit(){return tempIntDeposit;}
    public float getFloatDeposit(){return tempFloatDeposit;}
    
    public void setIntWithdraw(int a2) 
    {
        tempIntWithdraw = a2;
    }       
    public void setFloatWithdraw(float a2) 
    {
        tempFloatWithdraw = a2;
    }
    
    public int getIntWithdraw(){return tempIntWithdraw;}
    public float getFloatWithdraw(){return tempFloatWithdraw;}
}
