
/**
 *
 * @author Rohan_Kachewar
 */
class Locked extends States {

    private int state_id;
    
    public Locked() {
        state_id = 6;
    }
                   
    public int get_id()
    {
        return state_id;
    }
    
    public void unlock()
    {
        outputprocessorobj.unlock();
    }
}
