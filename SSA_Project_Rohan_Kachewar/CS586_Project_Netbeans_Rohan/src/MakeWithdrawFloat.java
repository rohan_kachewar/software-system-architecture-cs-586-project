
import java.math.BigDecimal;
import java.math.RoundingMode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
public class MakeWithdrawFloat extends MakeWithdraw{
 
    DataStore db;
    
    MakeWithdrawFloat(DataStore dbs) {
        db = dbs;
    }
    
    public void makeWithdraw()
    {
        DataStore tempDB=new TempDataStore();
        float tempWithdraw=tempDB.getFloatWithdraw();   //get withdraw amnt from temp db         
        float balance=db.getFloatBalance();   //get existing balance
        //Use of BigDecimal ensures less error prone floating point operations
        BigDecimal foo1 = new BigDecimal(balance);
        BigDecimal foo2 = new BigDecimal(tempWithdraw);
        BigDecimal foo3=foo1.subtract(foo2); //withdraw amount
        float totalamt = foo3.floatValue();                
        db.setBalance(totalamt);  //set new balance
    }
}
