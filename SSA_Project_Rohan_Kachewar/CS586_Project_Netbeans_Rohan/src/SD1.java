
/**
 *
 * @author Rohan_Kachewar
 */
public class SD1 extends StoreData {

    DataStore tempDB;
    DataStore db;
            
    public SD1(DataStore dbs) {
        tempDB = new TempDataStore();
        db = dbs;
    }
    
    public void storePIN()
    {        
        String pin =tempDB.getPIN();                
        db.setPIN(pin);        
    }
    
    public void storeBalance()
    {        
        int bal =tempDB.getBalance();                
        db.setBalance(bal);        
    }

    
}
