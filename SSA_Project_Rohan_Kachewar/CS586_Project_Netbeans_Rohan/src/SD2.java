/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rohan_Kachewar
 */
class SD2 extends StoreData {

    DataStore tempDB;
    DataStore db;
            
    public SD2(DataStore dbs) {
        tempDB = new TempDataStore();
        db = dbs;
    }
    
    public void storePIN()
    {        
        int pin =tempDB.getIntPIN();                
        db.setPIN(pin);        
    }
    
    public void storeBalance()
    {        
        float bal =tempDB.getFloatBalance();                
        db.setBalance(bal);        
    }
    
}
