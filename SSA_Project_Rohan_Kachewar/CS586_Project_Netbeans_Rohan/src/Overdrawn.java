
/**
 *
 * @author Rohan_Kachewar
 */
class Overdrawn extends States {

    private int state_id;
    
    public Overdrawn() {
        state_id = 5;
    }
                   
    public int get_id()
    {
        return state_id;
    }
    
    public void deposit()
    {
        outputprocessorobj.makeDeposit();
    }
    
    public void displayBalance()
    {
        outputprocessorobj.displayBalance();
    }
    
    public void lock()
    {
        outputprocessorobj.lock();
    }
    
    public void eject_card()
    {
        outputprocessorobj.eject_card();
    }
}
