
/**
 *
 * @author Rohan_Kachewar
 */
public class MakeDepInt extends MakeDeposit{
   DataStore db; 
    
    MakeDepInt(DataStore dbs) {
       db = dbs;
    }
    
    
    public void makeDeposit()
    {
        DataStore tempDB=new TempDataStore();
        int tempDeposit=tempDB.getIntDeposit();   //get deposit amnt        
        int balance=db.getBalance();   //get existing balance
        int totalamt=tempDeposit+balance;        
        db.setBalance(totalamt);  //set new balance
    }
    
    
}
